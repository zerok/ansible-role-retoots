# Ansible role: retoots

This is a simple role for installing
[retoots](https://github.com/zerok/retoots) on a server.
